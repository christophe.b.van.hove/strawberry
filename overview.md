#Interfaces:

#IP
-Features:
 -point-to-point delivery
 -error-free delivery
 -packet delivery
 -packet dropping
-Requests
 -send
-Indications
 -receive
-Calls
 -transmit
-Callbacks
 -transmit

#TCP
-Features:
 -point-to-point delivery
 -guaranteed delivery
 -stream delivery
 -packet dropping
-Requests
 -send
-Indications
-Calls
-Callbacks

#UDP
-Features:
 -point-to-point delivery
 -
-Requests
 -send
-Indications
-Calls
-Callbacks

#SIGFOX
-Features:
-Requests
 -send
-Indications
-Calls
-Callbacks

#Bluetooth
-Features:
-Requests
 -send
-Indications
-Calls
-Callbacks
