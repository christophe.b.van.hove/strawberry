defmodule Strawberry.Application do
  # See https://hexdocs.pm/elixir/Application.html
  # for more information on OTP Applications
  @moduledoc false

  use Application

  def start(_type, _args) do
    # List all child processes to be supervised

    children =
      if Regex.match?(~r/distributor@/, Atom.to_string(Node.self())) do
        [{Strawberry.Distributor.Supervisor, %{}}]
      else
        [{Strawberry.Slave, %{:address => Application.get_env(:strawberry, :distributor_ip)}}]
      end

    # See https://hexdocs.pm/elixir/Supervisor.html
    # for other strategies and supported options
    opts = [strategy: :one_for_one, name: Strawberry.Supervisor]
    Supervisor.start_link(children, opts)
  end
end
