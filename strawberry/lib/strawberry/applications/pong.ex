defmodule Strawberry.Pong do
  import Strawberry.Layer
  use Strawberry.Layer
  require Logger

  init_layer state do
    state =
      Strawberry.Util.state_check(
        state,
        :started,
        false,
        false
      )
  end

  # Client

  def start(pid, target) do
    GenServer.cast(pid, {:ping, target})
  end

  def stop(pid) do
    GenServer.call(pid, :stop)
  end

  # Server

  def handle_cast({:ping, target}, state) do
    Logger.debug("Sending ping to #{inspect(target)}")
    state.lower_mod.send(state.lower_layer, :ping, target)
    state = Map.put(state, :started, true)
    {:noreply, state}
  end

  on_deliver(:ping, origin, state) do
    if state.started do
      Logger.debug("Bouncing back ping ping from #{inspect(origin)}")
      state.lower_mod.send(state.lower_layer, :ping, origin)
    else
      Logger.debug("Receiving ping from #{inspect(origin)}")
    end

    {:noreply, state}
  end

  def handle_cast(:stop, state) do
    Logger.debug("Stopping")
    state = Map.put(state, :started, false)
    {:noreply, state}
  end

  on_indicate(message, origin, _deliverer, state) do
    Logger.info("Received response: #{inspect(message)} from #{inspect(origin)}")
    {:noreply, state}
  end
end
