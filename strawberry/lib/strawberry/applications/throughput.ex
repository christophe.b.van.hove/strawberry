defmodule Strawberry.Throughput do
  import Strawberry.Layer
  use Strawberry.Layer
  require Logger

  init_layer state do
    state =
      state
      |> Strawberry.Util.state_check(
        :packet_size,
        1,
        "Benchmarking with a size of 1 Byte"
      )
      |> Map.put(:started, false)
      |> Map.put(:last_packet, 1)
      |> Map.put(:received, [])
  end

  # Client

  def start(pid, target) do
    GenServer.cast(pid, {:ping, target})
  end

  def stop(pid) do
    GenServer.call(pid, :stop)
  end

  # Server

  def handle_cast(:beat, state) do
    if Map.get(state, :started) do
      target = state.target
      Logger.debug("Sending heartbeat to #{inspect(target)}")
      index = state.last_packet
      pure_size = :erlang_term.byte_size({:beat, index})
      padding = Strawberry.Padding.generate_pad(state.packet_size - pure_size)
      state.lower_layer.send(state.lower_layer, {:beat, index, padding}, target)
      GenServer.cast(self(), :beat)
      {:noreply, state}
    else
      {:noreply, state}
    end
  end

  def handle_cast({:start, target}, state) do
    newstate = Map.put(state, :started, true)
    newstate = Map.put(newstate, :target, target)
    GenServer.cast(self(), :beat)
    {:noreply, newstate}
  end

  def handle_cast(:stop, state) do
    state.lower_layer.send(state.lower_layer, :stop, state.target)
    newstate = Map.put(state, :started, false)
    {:noreply, newstate}
  end

  on_deliver({:beat, index, padding}, _origin, state) do
    size = :erlang_term.byte_size({:beat, index, padding})
    new_received = [{index, Time.utc_now(), size} | state.received]
    newstate = Map.put(state, :received, new_received)
    {:noreply, newstate}
  end

  on_deliver(:stop, _origin, state) do
    {maxID, total, firstTime, lastTime, totalSize} =
      List.foldl(state.received, 0, fn {index, time, size},
                                       {maxindex, currtotal, firstTime, lastTime, currsize} ->
        {max(index, maxindex), currtotal + 1,
         if(Time.compare(time, firstTime) == :lt, do: time, else: firstTime),
         if(Time.compare(time, lastTime) == :gt, do: time, else: lastTime), currsize + size}
      end)

    lost = maxID - total
    rate = totalSize / Time.diff(lastTime, firstTime)
    Logger.info("Lost #{inspect(lost)} packets out of #{inspect(total)}")
    Logger.info("Throughput is #{inspect(rate)} B/s")
    {:noreply, false}
  end
end
