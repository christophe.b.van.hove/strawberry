defmodule Strawberry.Component do
  require Logger

  defp has_timer?(spec) do
    case spec do
      {:__block__, _, insl} ->
        Enum.find(insl, fn statement ->
          case statement do
            {:every, _, _} -> true
            _ -> false
          end
        end)

      {:every, _, _} ->
        true

      _ ->
        false
    end
  end

  defp timer_id(spec) do
    :crypto.hash(:md4, inspect(spec)) |> Base.encode64()
  end

  defp has_init?(spec) do
    case spec do
      {:__block__, _, insl} ->
        Enum.find(insl, fn statement ->
          case statement do
            {:init, _, _} -> true
            _ -> false
          end
        end)

      {:init, _, _} ->
        true

      _ ->
        false
    end
  end

  defp put_timer(item) do
    case item do
      {:every, _, [time, _]} ->
        quote do
          state =
            Map.put(
              state,
              :timers,
              state.timers ++ [{unquote(timer_id(time)), unquote(time)}]
            )
        end

      _ ->
        nil
    end
  end

  # Inline reference and sending replacement functions

  defp stack_has_name?(name, network) do
    is_atom(name) && Enum.any?(network, fn {_layer, state} -> Map.get(state, :name) == name end)
  end

  defp replace_ref(item, scope, network) do
    case item do
      {:=, meta, [var, arg]} ->
        # IO.inspect(item)

        case var do
          {{:., _, [name, value]}, _, []} ->
            if stack_has_name?(name, network) do
              quote do
                Genserver.call(unquote(name), {:edit_state, unquote(value), unquote(arg)})
              end
            else
              {:=, meta, [var, arg]}
            end

          {name, b, nil} ->
            if :ets.member(scope, name) do
              # IO.puts(name)
              # IO.puts(is_atom(name))

              quote do
                state = Map.put(state, unquote(name), unquote(arg))
              end
            else
              {:=, meta, [{name, b, nil}, arg]}
            end

          _ ->
            {:=, meta, [var, arg]}
        end

      {:., meta, [var, arg]} ->
        if stack_has_name?(var, network) do
          quote do
            Genserver.call(unquote(var), {:get_state, arg})
          end
        else
          {:., meta, [var, arg]}
        end

      {name, b, nil} ->
        if :ets.member(scope, name) do
          quote do
            state.unquote(name)
          end
        else
          {name, b, nil}
        end

      item ->
        item
    end
  end

  defp replace_sending(ins) do
    case ins do
      {:trigger, _, [message]} ->
        quote do
          GenServer.cast(state.name, {:deliver, unquote(message), :self})
        end

      {:delayed_trigger, _, [time, message]} ->
        quote do
          :timer.apply_after(unquote(time), GenServer, :cast, [
            state.name,
            {:deliver, unquote(message), :self}
          ])
        end

      {:send, _, [target_alias, message]} ->
        {_, _, [target]} = target_alias

        if is_atom(target) do
          quote do
            if Map.has_key?(state.glomap, Module.concat(unquote(target), Supervisor)) do
              state.lower_mod.send(
                state.lower_layer,
                unquote(message),
                state.glomap[Module.concat(unquote(target), Supervisor)]
              )
            else
              Logger.error("Unknown target: #{inspect(unquote(target))}")
            end
          end
        else
        end

      {:cast, _, [message]} ->
        quote do
          state.lower_mod.send(state.lower_layer, unquote(message), :cast)
        end

      ins ->
        ins
    end
  end

  defp search_ref(block, scope, network) do
    case block do
      {name, meta, vars} ->
        {name, meta, vars}
        |> replace_ref(scope, network)
        |> replace_sending()
        |> (fn {name, meta, vars} ->
              {name, meta,
               if is_list(vars) do
                 Enum.map(vars, fn el ->
                   search_ref(el, scope, network)
                 end)
               else
                 vars
               end}
            end).()

      {keyword, bl} when is_atom(keyword) ->
        {keyword, search_ref(bl, scope, network)}

      l ->
        if is_list(l) do
          Enum.map(l, fn el ->
            search_ref(el, scope, network)
          end)
        else
          l
        end
    end
  end

  # Instruction handling functions

  defp get_from_do(block) do
    case block do
      [do: {:__block__, _, expression}] -> expression
      [do: expression] -> [expression]
    end
  end

  defp handle_instruction(ins, scope, network) do
    # IO.inspect(ins)

    case ins do
      {:on, _, [message, block]} ->
        # IO.inspect(block)
        expression = get_from_do(block)
        # IO.inspect(expression)

        quote do
          on_deliver(unquote(message), origin, state) do
            unquote_splicing(expression)
            {:noreply, state}
          end
        end

      {:init, _, [block]} ->
        # IO.inspect(block)
        expression = get_from_do(block)
        # IO.inspect(expression)

        quote do
          def init_component(state) do
            unquote_splicing(expression)
            state
          end
        end

      {:every, _, [time, block]} ->
        id = timer_id(time)

        quote do
          def handle_cast({reset_timer, unquote(id)}, state) do
            Logger.debug("Resetting Timer #{unquote(time)}")
            unquote_splicing(get_from_do(block))
            server = self()
            timeout = get_timeout(state, unquote(id))

            :timer.apply_after(timeout, GenServer, :cast, [
              server,
              {:reset_timer, unquote(id)}
            ])

            {:noreply, state}
          end
        end

      ins ->
        ins
    end
    |> search_ref(scope, network)

    # |> inspect()
    # |> IO.puts()
  end

  # Main component Macro

  defmacro component_map(name_network_list, state, do: spec) do
    for {name, network} <- name_network_list do
      Macro.expand(
        quote do
          component(unquote(name), unquote(state), unquote(network)) do
            unquote(spec)
          end
        end,
        __ENV__
      )
    end
  end

  defmacro component(name_aliases, state, network, do: spec) do
    # Timer auxiliary functions
    for name_alias <- if(is_list(name_aliases), do: name_aliases, else: [name_aliases]) do
      local_scope = :ets.new(:buckets_registry, [:set, :protected])
      # timers = :ets.new(:buckets_registry, [:set, :protected])
      {_, _, [name]} = name_alias

      x =
        quote do
          defmodule unquote(name) do
            import Strawberry.Layer
            use Strawberry.Layer
            require Logger

            def init(state) do
              Logger.debug("Starting component #{inspect(unquote(name))}")

              unquote_splicing(
                Enum.map(state, fn var ->
                  # IO.puts(inspect(var))

                  case var do
                    {:=, _, [name, decl]} ->
                      {id, _, _} = name
                      :ets.insert(local_scope, {id, nil})

                      quote do
                        state = Map.put(state, unquote(id), unquote(decl))
                      end

                    var ->
                      :ets.insert(local_scope, {var, nil})

                      quote do
                        state = Map.put(state, unquote(var), unquote(nil))
                      end
                  end
                end)
              )

              unquote(
                if has_timer?(spec) do
                  quote do
                    state = Map.put(state, :timers, [])
                  end
                end
              )

              unquote_splicing(
                if has_timer?(spec) do
                  # IO.inspect(spec)

                  case spec do
                    {:__block__, _, list} -> Enum.map(list, fn s -> put_timer(s) end)
                    spec -> [put_timer(spec)]
                  end
                else
                  [nil]
                end
              )

              unquote(
                if has_timer?(spec) do
                  quote do
                    state = start_timers(state)
                  end
                end
              )

              Strawberry.Util.state_present(state, :lower_layer, __MODULE__)

              unquote(
                if has_init?(spec) do
                  quote do
                    state = init_component(state)
                  end
                end
              )

              {:ok, state}
            end

            unquote(
              if has_timer?(spec) do
                quote do
                  defp get_timeout(state, id) do
                    {_id, timeout} =
                      Enum.find(state.timers, fn it ->
                        case it do
                          {id, _} -> true
                          _ -> false
                        end
                      end)

                    if is_number(timeout) do
                      timeout
                    else
                      timeout.()
                    end
                  end
                end
              end
            )

            unquote(
              if has_timer?(spec) do
                quote do
                  def start_timers(state) do
                    server = self()

                    for {id, _} <- state.timers do
                      timeout = get_timeout(state, id)
                      Logger.debug("Starting Timer #{inspect(id)}")
                      :timer.apply_after(timeout, GenServer, :cast, [server, {:reset_timer, id}])
                    end

                    state
                  end
                end
              end
            )

            unquote_splicing(
              case spec do
                {:__block__, _, insl} ->
                  Enum.map(insl, fn ins -> handle_instruction(ins, local_scope, network) end)

                ins ->
                  [handle_instruction(ins, local_scope, network)]
              end
            )

            on_deliver(_, _, _) do
              Logger.debug("unhandled message received")
              nil
            end
          end

          defmodule unquote(Module.concat(name, Supervisor)) do
            use Supervisor
            import Strawberry.LayerComposer
            require Logger

            def start_link(arg) do
              Supervisor.start_link(__MODULE__, arg, name: __MODULE__)
            end

            @impl true
            def init(state) do
              children =
                Enum.map({unquote(name), state} <~> unquote(network), fn {item, state} ->
                  %{id: state.name, start: {item, :start_link, [state]}}
                end)

              Logger.debug(
                "Starting component #{inspect(unquote(name))} using layers #{inspect(children)}"
              )

              Supervisor.init(children, strategy: :one_for_all)
            end
          end

          unquote(Module.concat(name, Supervisor))
        end

      Macro.to_string(x) |> IO.puts()
      x
    end
  end

  def send_to_component(component, message) do
    server =
      if Process.whereis(Strawberry.Distributor) do
        glomap = GenServer.call(Strawberry.Distributor, :get_glomap)
        # IO.inspect(glomap)
        {String.to_existing_atom(component), glomap[String.to_existing_atom(component)]}
      else
        component
      end

    GenServer.cast(server, {:deliver, message, :user})
  end
end
