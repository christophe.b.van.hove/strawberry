defmodule Strawberry.Distributor do
  use GenServer
  require Logger

  def start_link(state) do
    GenServer.start_link(__MODULE__, state, name: __MODULE__)
  end

  def init(_) do
    configs = List.flatten(Strawberry.Nodes.configs())
    {:ok, %{nodes: MapSet.new(), configs: configs}}
  end

  defp try_startup(state) do
    state =
      if MapSet.size(state.nodes) == length(state.configs) do
        Logger.info("Enough nodes found, distributing configs")
        glomap = Map.new(Enum.zip(state.configs, state.nodes))

        for {config, node} <- glomap do
          Logger.info("sending to #{inspect(node)}")
          GenServer.call({Strawberry.Slave, node}, {:initialize, config, %{glomap: glomap}})
        end

        Map.put(state, :glomap, glomap)
      else
        Logger.info(
          "Not yet enough nodes: #{inspect(MapSet.size(state.nodes))} out of #{
            inspect(length(state.configs))
          }"
        )

        state
      end

    state
  end

  def handle_cast({:node_present, node}, state) do
    state =
      state
      |> Map.put(:nodes, MapSet.put(state.nodes, node))
      |> try_startup()

    {:noreply, state}
  end

  def handle_call(:get_glomap, _from, state) do
    if(state[:glomap]) do
      {:reply, state[:glomap], state}
    else
      %{}
    end
  end
end

defmodule Strawberry.Distributor.Supervisor do
  use Supervisor

  def main(_) do
    Strawberry.Distributor.Supervisor.start_link([])
  end

  def start_link(_) do
    Supervisor.start_link(__MODULE__, [], name: __MODULE__)
  end

  def init(_) do
    children = [{Strawberry.Distributor, []}]
    Supervisor.init(children, strategy: :one_for_all)
  end
end
