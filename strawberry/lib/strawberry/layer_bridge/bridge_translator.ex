defmodule Strawberry.BridgeTranslator do
  require Logger
  use GenServer

  def start_link(state) do
    GenServer.start_link(__MODULE__, state, name: state.name)
  end

  def init(state) do
    state =
      state
      |> Strawberry.Util.state_check(
        :id,
        0,
        "Bridge Tranlator ID was not set"
      )
      |> Strawberry.Util.state_check(
        :direction,
        true,
        "Setting direction to down"
      )

    {:ok, state}
  end

  def send(pid, message, target) do
    GenServer.cast(pid, {:send, message, target})
  end

  def deliver(pid, message, origin) do
    GenServer.cast(pid, {:deliver, message, origin})
  end

  def redistribute(pid, message, origin) do
    GenServer.cast(pid, {:redristibute, message, origin})
  end

  def handle_cast({:send, message, target}, state) do
    if state.direction do
      GenServer.cast(state.lower_layer, {{:send, state.id}, message, target})
    else
      GenServer.cast(state.lower_layer, {:send, message, target})
    end

    {:noreply, state}
  end

  def handle_cast({:deliver, message, origin}, state) do
    if state.direction do
      GenServer.cast(state.upper_layer, {{:deliver, state.id}, message, origin})
    else
      GenServer.cast(state.upper_layer, {:deliver, message, origin})
    end

    {:noreply, state}
  end

  def handle_cast({:redistribute, message, origin}, state) do
    if state.direction do
      GenServer.cast(state.upper_layer, {:send, message, origin})
    else
      GenServer.cast(state.lower_layer, {:deliver, message, origin})
    end
  end

  def handle_cast({:indicate, message, origin, deliverer}, state) do
    for {other, other_mod} <- [
          {state.upper_layer, state.upper_mod},
          {state.lower_layer, state.lower_mod}
        ] do
      if other != deliverer do
        other_mod.indicate(other, message, origin, state.name)
      end
    end

    {:noreply, state}
  end
end
