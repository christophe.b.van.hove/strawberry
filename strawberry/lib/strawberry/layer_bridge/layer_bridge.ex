defmodule Strawberry.LayerBridge do
  import Strawberry.Layer
  use Strawberry.Layer
  require Logger
  # modes:
  # retransmit: each delivered message is sent again to the other stack
  # decide: each message is passed to a lambda, which gives the id of the stack to use
  # spread: each message is sent to each layer

  init_layer(state) do
    state =
      Strawberry.Util.state_check(
        state,
        :decide_lambda,
        fn send_id, message ->
          for id <- 0..length(state.layers) do
            if id != send_id do
              {id, message}
            end
          end
        end,
        false
        # "No decider lambda has been provided, assuming a spread model"
      )
  end

  def handle_cast({{:send, id}, message, target}, state) do
    redistribute_message(message, target, state, id)
    {:noreply, state}
  end

  def handle_cast({:send, message, target}, state) do
    redistribute_message(message, target, state)
    {:noreply, state}
  end

  def handle_cast({{:deliver, id}, message, origin}, state) do
    redistribute_message(message, origin, state, id)
    {:noreply, state}
  end

  def handle_cast({:deliver, message, origin}, state) do
    redistribute_message(message, origin, state)
    {:noreply, state}
  end

  def redistribute_message(message, origin_or_target, state, id \\ 0) do
    responses = state.decide_lambda(id, message)

    if is_list(responses) do
      for {id, newmessage} <- responses do
        Strawberry.BridgeTranslator.redistribute(state.layers[id], message, origin_or_target)
      end
    else
      {id, newmessage} = responses
      Strawberry.BridgeTranslator.redistribute(state.layers[id], newmessage, origin_or_target)
    end
  end

  on_indicate(message, origin, deliverer, state) do
    for {other, other_mod} <- [
          {state.upper_layer, state.upper_mod},
          {state.lower_layer, state.lower_mod}
        ] do
      if other != deliverer do
        other_mod.indicate(other, message, origin, state.name)
      end
    end

    {:noreply, state}
  end
end
