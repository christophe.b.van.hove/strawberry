defmodule Strawberry.LayerComposer do
  use Private
  # Layer1 <~> Layer2 <~> Layer3 
  # Layer4 = Layer1 <~> Layer2 <~> Layer3 
  # Layer1 multistack(LayerA2 <~> LayerA3 | LayerB2 <~> LayerA3) Layer4
  # Layer1 <~> Api ~> Layer2 ~> Layer3 ~> Api <~> Layer4
  # 
  # 
  private do
    def get_unique_name() do
      Snowflake.next_id() |> elem(1) |> Integer.to_string() |> String.to_atom()
    end

    def atom_layer() do
      {Strawberry.Api, %{name: get_unique_name()}}
    end

    def is_layer(object) do
      try do
        {name, options} = object
        is_atom(name) && is_map(options)
      rescue
        _ -> false
      end
    end

    def is_stack(stack) do
      try do
        Enum.all?(stack, fn x -> is_layer(x) end)
      rescue
        _ -> false
      end
    end

    def is_multistack(stacks) do
      try do
        Enum.all?(stacks, fn x -> is_stack(x) end)
      rescue
        _ -> false
      end
    end

    def link_upper({item, state}, {upper, upper_state}) do
      upper_state = Strawberry.Util.state_check(upper_state, :name, get_unique_name(), nil)
      state = Map.put(state, :upper_layer, upper_state.name)
      state = Map.put(state, :upper_mod, upper)
      {{item, state}, {upper, upper_state}}
    end

    def link_lower({item, state}, {lower, lower_state}) do
      lower_state = Strawberry.Util.state_check(lower_state, :name, get_unique_name(), nil)
      state = Map.put(state, :lower_layer, lower_state.name)
      state = Map.put(state, :lower_mod, lower)
      {{item, state}, {lower, lower_state}}
    end

    def split_left_right(left, right) do
      {if is_list(left) do
         Enum.split(left, length(left) - 1)
       else
         {[], [left]}
       end,
       if is_list(right) do
         Enum.split(right, 1)
       else
         {[right], []}
       end}
    end

    def split_ends(list) do
      # IO.inspect(list)
      {left, temp} = Enum.split(list, 1)
      {middle, right} = Enum.split(temp, length(temp) - 1)
      # IO.inspect({left, middle, right})
      {left, middle, right}
    end
  end

  def left <~> right do
    # IO.puts("inspecting")
    # IO.inspect(left)
    # IO.inspect(right)
    {{left_left, [right_left]}, {[left_right], right_right}} = split_left_right(left, right)
    # IO.inspect({{left_left, [right_left]}, {[left_right], right_right}})
    {right_left, left_right} = link_lower(right_left, left_right)
    {left_right, right_left} = link_upper(left_right, right_left)
    List.flatten([left_left, right_left, left_right, right_right])
  end

  def one_way(items) do
    if is_stack(items) do
      {[left], middle, [right]} = split_ends(items)
      {first, left} = link_lower(atom_layer(), left)
      {right, last} = link_lower(right, atom_layer())
      {right, first} = link_upper(right, first)
      {last, left} = link_lower(last, left)
      {last, left} = link_upper(last, left)
      List.flatten([first, left, middle, right, last])
    else
      atom_layer() <~> items <~> atom_layer()
    end
  end

  def left ~> right do
    # IO.puts("~>") 
    # IO.inspect({left, right})
    {{left_left, [right_left]}, {[left_right], right_right}} = split_left_right(left, right)
    {right_left, left_right} = link_lower(right_left, left_right)
    {right_left, left_right} = link_upper(right_left, left_right)
    # IO.inspect(List.flatten([left_left, right_left, left_right, right_right]))
    List.flatten([left_left, right_left, left_right, right_right])
  end

  def first ||| second do
    [first_formatted, second_formatted] =
      Enum.map([first, second], fn el ->
        if is_multistack(el) do
          el
        else
          if is_stack(el) do
            [el]
          else
            if is_layer(el) do
              [[el]]
            end
          end
        end
      end)

    first_formatted ++ second_formatted
  end

  def multistack(stacks) when is_list(stacks) do
    multistack(%{}, stacks, %{})
  end

  def multistack(options, stacks) when is_map(options) and is_list(stacks) do
    multistack(options, stacks, %{})
  end

  def multistack(stacks, options) when is_list(stacks) and is_map(options) do
    multistack(%{}, stacks, options)
  end

  def multistack(options_top, stacks, options_bottom)
      when is_map(options_top) and is_list(stacks) and is_map(options_bottom) do
    ended_stacks =
      for id <- 1..length(stacks) do
        upper_name = get_unique_name()
        lower_name = get_unique_name()

        {{Strawberry.BridgeTranslator, %{name: upper_name, id: id, direction: true}}
         <~> Enum.at(stacks, id - 1)
         <~> {Strawberry.BridgeTranslator, %{name: lower_name, id: id, direction: false}},
         upper_name, lower_name}
      end

    upper_ids = Enum.map(ended_stacks, fn {_stack, upper_name, _lower_name} -> upper_name end)
    lower_ids = Enum.map(ended_stacks, fn {_stack, _upper_name, lower_name} -> lower_name end)

    stack =
      List.flatten(Enum.map(ended_stacks, fn {stack, _upper_name, _lower_name} -> stack end))

    upper_adapter_name = get_unique_name()
    lower_adapter_name = get_unique_name()

    upper_adapter =
      {Strawberry.BridgeTranslator, %{name: upper_adapter_name, id: 0, direction: false}}

    lower_adapter =
      {Strawberry.BridgeTranslator, %{name: lower_adapter_name, id: 0, direction: true}}

    appended_options_top = options_top |> Map.put(:layers, [upper_adapter_name] ++ upper_ids)

    appended_options_bottom =
      options_bottom |> Map.put(:layers, [lower_adapter_name] ++ lower_ids)

    [upper_adapter, {Strawberry.LayerBridge, appended_options_top}] ++
      stack ++ [{Strawberry.LayerBridge, appended_options_bottom}, lower_adapter]
  end
end
