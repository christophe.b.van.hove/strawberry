defmodule Strawberry.AckRetransmitter do
  import Strawberry.Layer
  use Strawberry.Layer
  require Logger

  init_layer state do
    state =
      state
      |> Strawberry.Util.state_check(
        :ack_timeout,
        1,
        "acknowledgment timeout is 1 milisecond"
      )
      |> Strawberry.Util.state_check(
        :window_size,
        1,
        "sliding window size is 1"
      )
      |> Map.put(:buffer, :queue.new())
      |> Map.put(:current_id, 0)
  end

  # defp increase_id(state) do
  #  Map.put(state, :current_id, state.current_id + 1)
  # end

  # defp add_to_queue(state, item) do
  #  new_queue = :queue.in(state.queue, state.current_id)
  #  Map.put(state, :buffer, new_queue)
  # end

  on_send(message, target, state) do
    state.lower_mod.send(state.lower_layer, message, target)
    {:noreply, state}
  end

  on_deliver({:ack, _id}, _origin, state) do
    {:noreply, state}
  end

  on_deliver(message, origin, state) do
    state.upper_mod.deliver(state.upper_layer, message, origin)
    {:noreply, state}
  end

  on_indicate(message, origin, deliverer, state) do
    for {other, other_mod} <- [
          {state.upper_layer, state.upper_mod},
          {state.lower_layer, state.lower_mod}
        ] do
      if other != deliverer do
        other_mod.indicate(other, message, origin, state.name)
      end
    end

    {:noreply, state}
  end

  def handle_info(:timeout, state) do
    # set_switch(state)

    {:noreply, state, 100}
  end
end
