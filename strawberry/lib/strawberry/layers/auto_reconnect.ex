defmodule Strawberry.AutoReconnect do
  import Strawberry.Layer
  use Strawberry.Layer
  require Logger

  init_layer state do
    state =
      state
      |> Strawberry.Util.state_check(
        :reconnect_delay,
        1,
        "reconnect delay set as 1 second"
      )
      |> Map.put(:status, false)
      |> reset_timer()
  end

  # Timer for reconnect pushes

  def reset_timer(state) do
    limiter = self()

    timer =
      Task.async(fn ->
        if !state.status do
          :timer.sleep(state.reconnect_delay)
          indicate(state.name, :connect)
          GenServer.call(limiter, :reset_timer)
        end
      end)

    Map.put(state, :timer, timer)
  end

  def handle_call({:reset_timer}, _from, state) do
    state = reset_timer(state)
    {:noreply, state}
  end

  # Handling indicators

  on_indicate(
    :disconnected,
    _origin,
    _deliverer,
    state
  ) do
    state = Map.put(state, :status, false)
    reset_timer(state)
    {:noreply, state}
  end

  on_indicate(
    :connected,
    _origin,
    _deliverer,
    state
  ) do
    state = Map.put(state, :status, true)
    {:noreply, state}
  end

  on_indicate(message, origin, deliverer, state) do
    for {other, other_mod} <- [
          {state.upper_layer, state.upper_mod},
          {state.lower_layer, state.lower_mod}
        ] do
      if other != deliverer do
        other_mod.indicate(other, message, origin, state.name)
      end
    end

    {:noreply, state}
  end

  on_send(message, target, state) do
    state.lower_mod.send(state.lower_layer, message, target)
    {:noreply, state}
  end

  on_deliver(message, origin, state) do
    state.upper_mod.deliver(state.upper_layer, message, origin)
    {:noreply, state}
  end
end
