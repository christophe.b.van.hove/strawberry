defmodule Strawberry.ConstantErrorRate do
  import Strawberry.Layer
  use Strawberry.Layer
  require Logger

  init_layer state do
    state =
      Strawberry.Util.state_check(
        state,
        :drop_fraction,
        0.00001,
        "Constant Error rate assumed of 10^-5"
      )
  end

  def drop?(message, state) do
    size = Strawberry.Padding.find_size(message)

    :rand.uniform() <= (state.drop_fraction * size)
  end

  on_send(message, target, state) do
    if drop?(message, state) do
      {:noreply, state}
    else
      state.lower_mod.send(state.lower_layer, message, target)
      {:noreply, state}
    end
  end

  on_deliver(message, origin, state) do
    if drop?(message, state) do
      {:noreply, state}
    else
      state.upper_mod.deliver(state.upper_layer, message, origin)
      {:noreply, state}
    end
  end

  # pass-through indicate
  on_indicate(message, origin, deliverer, state) do
    for {other, other_mod} <- [
          {state.upper_layer, state.upper_mod},
          {state.lower_layer, state.lower_mod}
        ] do
      if other != deliverer do
        other_mod.indicate(other, message, origin, state.name)
      end
    end

    {:noreply, state}
  end
end
