defmodule Strawberry.RateLimiter do
  import Strawberry.Layer
  use Strawberry.Layer
  require Logger

  init_layer state do
    state =
      Strawberry.Util.state_check(
        state,
        :rate,
        1,
        "Rate Limiter configured without rate, assuming 1b/s"
      )
  end

  def delay(item, state) do
    size = Strawberry.Padding.find_size(item)
    delay = Kernel.round(size / state.rate * 0.125)
    :timer.sleep(delay)
  end

  on_send(message, target, state) do
    delay(message, state)
    state.lower_mod.send(state.lower_layer, message, target)
    {:noreply, state}
  end

  on_deliver(message, origin, state) do
    delay(message, state)
    state.upper_mod.deliver(state.upper_layer, message, origin)
    {:noreply, state}
  end

  # pass-through indicate
  on_indicate(message, origin, deliverer, state) do
    for {other, other_mod} <- [
          {state.upper_layer, state.upper_mod},
          {state.lower_layer, state.lower_mod}
        ] do
      if other != deliverer do
        other_mod.indicate(other, message, origin, state.name)
      end
    end

    {:noreply, state}
  end

  def handle_info(:timeout, state) do
    Logger.info("Timout occurring")
    {:noreply, state}
  end
end
