defmodule Strawberry.Layer do
  @type server :: {:global, term} | atom | {:via, module, term}
  @type cast_reply ::
          {:noreply, map}
          | {:noreply, map, timeout() | :hibernate}
          | {:stop, reason :: term(), map}
  @callback send(pid, any(), server) :: :ok
  @callback deliver(pid, any(), server) :: :ok
  require Logger
  # @callback handle_cast({:send, {any, server}}, map) :: cast_reply
  # @callback handle_cast({:deliver, any}, map) :: cast_reply

  defmacro __using__(_opts) do
    quote do
      use GenServer
      require Logger
      @behaviour Strawberry.Layer

      def start_link(state) do
        GenServer.start_link(__MODULE__, state, name: state.name)
      end

      def send(pid, message, target) do
        GenServer.cast(pid, {:send, message, target})
      end

      def deliver(pid, message, origin) do
        GenServer.cast(pid, {:deliver, message, origin})
      end

      def indicate(pid, message, origin, deliverer) do
        GenServer.cast(pid, {:indicate, message, origin, deliverer})
      end

      def indicate(pid, message, origin) do
        GenServer.cast(pid, {:indicate, message, origin, origin})
      end

      def indicate(name, message) do
        GenServer.cast(name, {:indicate, message, name, name})
      end

      def edit_state(pid, param, new_value) do
        GenServer.call(pid, {:edit_state, param, new_value})
      end

      def get_state(pid, param) do
        GenServer.call(pid, {:get_state, param})
      end

      def handle_call({:edit_state, param, new_value}, {pid, _id}, state) do
        if Map.has_key?(state, param) && Process.alive?(pid) do
          new_state = Map.put(state, param, new_value)
          {:reply, :error, new_state}
        else
          {:reply, :error, state}
        end
      end

      def handle_call({:get_state, param}, {pid, _id}, state) do
        if Process.alive?(pid) do
          {:reply, state[param], state}
        else
          {:reply, :error, state}
        end
      end

      defoverridable send: 3
      defoverridable deliver: 3
      defoverridable indicate: 2
      defoverridable indicate: 3
      defoverridable indicate: 4
    end
  end

  defmacro init_layer(state, do: expression) do
    quote do
      def init(unquote(state)) do
        Strawberry.Util.state_present(unquote(state), :upper_layer, __MODULE__)
        Strawberry.Util.state_present(unquote(state), :lower_layer, __MODULE__)
        unquote(expression)
        {:ok, unquote(state)}
      end
    end
  end

  defmacro on_deliver(message, origin, state, do: expression) do
    quote do
      def handle_cast({:deliver, unquote(message), unquote(origin)}, unquote(state)) do
        unquote(expression)
        # {:noreply, unquote(state)}
      end
    end
  end

  defmacro on_send(message, target, state, do: expression) do
    quote do
      def handle_cast({:send, unquote(message), unquote(target)}, unquote(state)) do
        unquote(expression)
        # {:noreply, unquote(state)}
      end
    end
  end

  defmacro on_indicate(message, origin, deliverer, state, do: expression) do
    quote do
      def handle_cast(
            {:indicate, unquote(message), unquote(origin), unquote(deliverer)},
            unquote(state)
          ) do
        unquote(expression)
        # {:noreply, unquote(state)}
      end
    end
  end
end
