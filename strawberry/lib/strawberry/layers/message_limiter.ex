defmodule Strawberry.MessageLimiter do
  import Strawberry.Layer
  use Strawberry.Layer
  require Logger

  init_layer state do
    state =
      state
      |> Strawberry.Util.state_check(
        :n_of_packets,
        1,
        "Message limiter configured without number of packets, assuming 1"
      )
      |> Strawberry.Util.state_check(
        :time_limit,
        1,
        "Message limiter configured without time segment, assuming 1 second"
      )
      |> Map.put(:current_used, 0)
      |> reset_timer()
  end

  def reset_timer(state) do
    state = Map.put(state, :current_used, 0)
    limiter = self()

    timer =
      Task.async(fn ->
        :timer.sleep(state.time_limit)
        GenServer.call(limiter, :reset_timer)
      end)

    Map.put(state, :timer, timer)
  end

  def handle_call({:reset_timer}, _from, state) do
    new_state = reset_timer(state)
    {:noreply, new_state}
  end

  on_send(message, target, state) do
    if state.n_of_packets >= state.current_used do
      Task.await(state.timer)
    end

    state.lower_mod.send(state.lower_layer, message, target)
    state = Map.put(state, :current_used, state.current_used + 1)
    {:noreply, state}
  end

  on_deliver(message, origin, state) do
    if state.n_of_packets >= state.current_used do
      Task.await(state.timer)
    end

    state.lower_mod.deliver(state.lower_layer, message, origin)
    state = Map.put(state, :current_used, state.current_used + 1)
    {:noreply, state}
  end

  # pass-through indicate
  on_indicate(message, origin, deliverer, state) do
    for {other, other_mod} <- [
          {state.upper_layer, state.upper_mod},
          {state.lower_layer, state.lower_mod}
        ] do
      if other != deliverer do
        other_mod.indicate(other, message, origin, state.name)
      end
    end

    {:noreply, state}
  end
end
