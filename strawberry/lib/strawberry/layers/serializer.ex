defmodule Strawberry.Serializer do
  import Strawberry.Layer
  use Strawberry.Layer
  require Logger

  init_layer state do
  end

  on_send(message, target, state) do
    {st, serialized_message} = :msgpack.pack(message)

    if st == :ok do
      state.lower_mod.send(state.lower_layer, serialized_message, target)
    else
      indicate(state.name, :unserializable)
    end

    {:noreply, state}
  end

  on_deliver(message, origin, state) do
    {:ok, deserialized_message} = :msgpack.unpack(message)
    state.upper_mod.deliver(state.upper_layer, deserialized_message, origin)
    {:noreply, state}
  end

  # pass-through indicate
  on_indicate(message, origin, deliverer, state) do
    for {other, other_mod} <- [
          {state.upper_layer, state.upper_mod},
          {state.lower_layer, state.lower_mod}
        ] do
      if other != deliverer do
        other_mod.indicate(other, message, origin, state.name)
      end
    end

    {:noreply, state}
  end
end
