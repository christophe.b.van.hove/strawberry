defmodule Strawberry.SimpleRetransmitter do
  import Strawberry.Layer
  use Strawberry.Layer
  require Logger

  init_layer state do
    state =
      state
      |> Strawberry.Util.state_check(
        :ack_timeout,
        1000,
        "acknowledgment timeout is 1 second"
      )
      |> Map.put(:buffer, :queue.new())
      |> Map.put(:transmitted, true)
      |> Map.put(:current_id, 0)
      |> Map.put(:last_id_seen, nil)
  end

  defp increase_id(state) do
    Map.put(state, :current_id, state.current_id + 1)
  end

  defp add_to_queue(state, {message, target}) do
    new_queue = :queue.in(state.queue, {message, target})
    Map.put(state, :buffer, new_queue)
  end

  defp pop_front(state) do
    {_v, new_queue} = :queue.out(state.queue)
    Map.put(state, :buffer, new_queue)
  end

  defp start_timer(state) do
    retr = self()
    id = state.current_id

    timer =
      Task.async(fn ->
        :timer.sleep(state.ack_timeout)
        GenServer.call(retr, {:timeout, id})
      end)

    Map.put(state, :timer, timer)
    state
  end

  defp retransmit(state) do
    {message, target} = :queue.get(state.buffer)
    id = state.current_id
    state.lower_mod.send(state.lower_layer, {message, id}, target)
    state
  end

  def handle_call({:timeout, id}, state) do
    state =
      if id == state.current_id do
        state
        |> retransmit()
        |> start_timer()
      else
        state
      end

    {:noreply, state}
  end

  on_send(message, target, state) do
    state =
      if state.transmitted do
        state
        |> increase_id()
        |> add_to_queue({message, target})
        |> Map.put(:transmitted, false)
        |> retransmit()
        |> start_timer()
      else
        add_to_queue(state, message)
      end

    {:noreply, state}
  end

  on_deliver({id, :ack}, _origin, state) do
    state =
      if id == state.current_id do
        state
        |> increase_id()
        |> pop_front()
        |> retransmit()
      else
        state
      end

    {:noreply, state}
  end

  on_deliver({message, id}, origin, state) do
    state =
      if id == state.last_id_seen + 1 do
        state.lower_mod.send(state.lower_layer, {id, :ack}, origin)
        state.upper_mod.deliver(state.upper_layer, message, origin)
        Map.put(state, :last_id_seen, id)
      else
        if id <= state.last_id_seen do
          state.lower_mod.send(state.lower_layer, {id, :ack}, origin)
        end

        state
      end

    {:noreply, state}
  end

  on_indicate(message, origin, deliverer, state) do
    for {other, other_mod} <- [
          {state.upper_layer, state.upper_mod},
          {state.lower_layer, state.lower_mod}
        ] do
      if other != deliverer do
        other_mod.indicate(other, message, origin, state.name)
      end
    end

    {:noreply, state}
  end
end
