defmodule Strawberry.SizeLimiter do
  import Strawberry.Layer
  use Strawberry.Layer
  require Logger

  init_layer state do
    state =
      Strawberry.Util.state_check(
        state,
        :packet_size,
        1,
        "Max Packet size set as 1 Byte (will most likely not work"
      )
  end

  on_send(message, target, state) do
    if :erlang_term.byte_size(message) < state.packet_size do
      state.lower_mod.send(state.lower_layer, message, target)
    else
      indicate(state.name, :packet_too_large)
    end

    {:noreply, state}
  end

  on_deliver(message, origin, state) do
    if :erlang_term.byte_size(message) < state.packet_size do
      state.upper_mod.deliver(state.upper_layer, message, origin)
    end

    {:noreply, state}
  end

  on_indicate(message, origin, deliverer, state) do
    for {other, other_mod} <- [
          {state.upper_layer, state.upper_mod},
          {state.lower_layer, state.lower_mod}
        ] do
      if other != deliverer do
        other_mod.indicate(other, message, origin, state.name)
      end
    end

    {:noreply, state}
  end
end
