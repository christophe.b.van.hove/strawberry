defmodule Strawberry.States do
  import Strawberry.Layer
  use Strawberry.Layer
  require Logger

  ## States
  # connected_and_in_range
  # disconnected_and_out_of_range
  # disconnected_and_in_range

  init_layer state do
    state =
      Strawberry.Util.state_check(
        state,
        :connect_delay,
        1,
        "Wifi state configured without a connect delay, assuming 1 second"
      )

    {:ok, {:disconnected_and_in_range, state}}
  end

  # Connection Indications

  on_indicate(:moving_out_of_range, _origin, _deliverer, {:connected_and_in_range, state}) do
    indicate(state.name, :disconnected)
    {:noreply, {:disconnected_and_out_of_range, state}}
  end

  on_indicate(:moving_in_range, _origin, _deliverer, {:disconnected_and_out_of_range, state}) do
    # indicate(state.name, :connect)
    {:noreply, {:disconnected_and_in_range, state}}
  end

  on_indicate(:connect, _origin, _deliverer, {:disconnected_and_in_range, state}) do
    :timer.sleep(state.connect_delay)
    indicate(state.name, :connected)
    {:noreply, {:connected_and_in_range, state}}
  end

  # pass-through indicate
  on_indicate(message, origin, deliverer, state) do
    for {other, other_mod} <- [
          {state.upper_layer, state.upper_mod},
          {state.lower_layer, state.lower_mod}
        ] do
      if other != deliverer do
        other_mod.indicate(other, message, origin, state.name)
      end
    end

    {:noreply, state}
  end

  # Send and Deliver

  on_send(message, target, {:connected_and_in_range, state}) do
    state.lower_mod.send(state.lower_layer, message, target)
    {:noreply, {:connected_and_in_range, state}}
  end

  on_deliver(message, origin, {:connected_and_in_range, state}) do
    state.upper_mod.deliver(state.upper_layer, message, origin)
    {:noreply, {:connected_and_in_range, state}}
  end

  on_send(_message, _target, {connection_state, state}) do
    indicate(state.name, :disconnected, state.name)
    {:noreply, {connection_state, state}}
  end

  on_deliver(_message, _origin, {connection_state, state}) do
    {:noreply, {connection_state, state}}
  end
end
