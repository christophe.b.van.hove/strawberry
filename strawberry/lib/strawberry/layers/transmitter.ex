defmodule Strawberry.Transmitter do
  import Strawberry.Layer
  use Strawberry.Layer
  require Logger

  def init(state) do
    state =
      state
      |> (fn st ->
            if !Map.has_key?(st, :nodes) do
              Map.put(st, :nodes, MapSet.new())
            else
              st
            end
          end).()
      |> (fn st ->
            if is_list(st.nodes) do
              MapSet.new(st.nodes)
            else
              st
            end
          end).()
      |> Strawberry.Util.state_check(
        :learning,
        true,
        "assuming transmitter can remember connections"
      )
      |> Strawberry.Util.state_check(
        :fixed,
        false,
        "assuming transmitter can send to connections not yet used"
      )
      |> Strawberry.Util.state_check(
        :cast,
        true,
        "assuming transmitter can cast over all known nodes"
      )
      |> Strawberry.Util.state_present(:upper_layer, __MODULE__)

    {:ok, state}
  end

  on_send(message, :cast, state) do
    if state.cast do
      for node <- state.nodes do
        Strawberry.Transmitter.send(self(), message, node)
      end
    else
      Logger.warn("Multicast not allowed")
    end

    {:noreply, state}
  end

  on_send(message, target, state) do
    Logger.debug("Sending packet #{inspect(message)} to #{inspect(target)}")

    state =
      if !state.fixed || MapSet.member?(state.nodes, target) do
        if is_pid(target) do
          GenServer.cast(target, {:transmit, message, self()})
        else
          GenServer.cast({state.name, target}, {:transmit, message, self()})
        end

        if state.learning do
          Map.put(state, :nodes, MapSet.put(state.nodes, target))
        else
          state
        end
      else
        state
      end

    {:noreply, state}
  end

  def handle_cast({:transmit, any, sender}, state) do
    Logger.debug("Receiving packet #{inspect(any)} from #{inspect(sender)}")
    state.upper_mod.deliver(state.upper_layer, any, sender)
    {:noreply, state}
  end

  # pass-through indicate
  on_indicate(message, origin, deliverer, state) do
    if state.upper_layer != deliverer do
      state.upper_mod.indicate(state.upper_layer, message, origin, state.name)
    end

    {:noreply, state}
  end
end
