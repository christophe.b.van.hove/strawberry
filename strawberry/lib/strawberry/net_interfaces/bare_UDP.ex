defmodule Strawberry.BareUDP do
  import Strawberry.Layer
  use Strawberry.Layer
  require Logger

  init_layer state do
    state = Map.put(state, :servers, %{})
  end

  def handle_call(:get_address_port, _from, state) do
    name = Atom.to_string(Node.self())
    [_hostname, address] = Regex.split(~r{@}, name, parts: 2)
    address_split = Regex.split(~r{\.}, address, parts: 4) |> Enum.map(&(Integer.parse(&1)|>elem(0)))|> List.to_tuple()
    server = Socket.UDP.open!()
    :ok = :inet.setopts(server,[{:active,true}])
    {:ok, {_, port}} = Socket.local(server)
    Logger.debug("address and port #{inspect(address_split)} #{inspect(port)}")
    {:reply, {address_split, port}, state}
  end

  on_send(message, target, state) do
    state =
      if(Map.has_key?(state.servers, target)) do
        state
      else
        {address, port} = GenServer.call({state.name, target}, :get_address_port)
        server = Socket.UDP.open!()
        Map.put(state, :servers, Map.put(state.servers, target, {server, address, port}))
      end

    {server, addr, port} = Map.get(state.servers, target)
    Logger.debug("server, address and port #{inspect(server)} #{inspect(addr)} #{inspect(port)}")
    Socket.Datagram.send!(server, :erlang.term_to_binary(message), {addr, port})
    {:noreply, state}
  end

  def handle_info({:udp, _socket, _ip, _fromport, message}, state) do
    state.upper_mod.deliver(state.upper_layer, :erlang.binary_to_term(message), nil)
    {:noreply, state}
  end
end
