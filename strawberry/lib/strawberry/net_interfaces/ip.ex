defmodule Strawberry.IP do
  import Strawberry.LayerComposer

  # Ethernet frame spec
  def layer do
    one_way(
      {Strawberry.SizeLimiter, %{packet_size: 1500}}
      # 10Gb/sec
      ~> {Strawberry.RateLimiter, %{rate: :math.pow(10, 10)}}
      # 802 spec
      ~> {Strawberry.ConstantErrorRate, %{drop_fraction: :math.pow(10, -8)}}
    )
    <~> {Strawberry.Transmitter, %{name: :ip_transmitter}}
  end
end
