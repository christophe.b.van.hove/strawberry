defmodule Strawberry.MultiBluetooth do
  import Strawberry.LayerComposer

  def layer([return_link | other_links]) do
    multistack(
      %{:decider => fn id, message -> {1, message} end},
      other_links
      |> Enum.map(fn link_name -> {Strawberry.Bluetooth, %{name: link_name}} end)
      |> List.foldl({Strawberry.Bluetooth, %{name: return_link}}, fn elem, acc -> acc ||| elem end)
    )
  end
end
