defmodule Strawberry.SimpleTransmitter do
  import Strawberry.LayerComposer

  # Ethernet frame spec
  def layer do
    {Strawberry.Transmitter, %{name: :simple_transmitter}}
  end

  def udp_layer do
    {Strawberry.BareUDP, %{name: :bare_udp}}
  end

  def stacked_layer(n) do
    List.foldl(
      for x <- 1..(n - 1) do
        x
      end,
      {Strawberry.Transmitter, %{name: :simple_transmitter}},
      fn _x, acc -> {Strawberry.Api, %{}} <~> acc end
    )
  end
  def stacked_layer_udp(n) do
    List.foldl(
      for x <- 1..(n - 1) do
        x
      end,
      {Strawberry.BareUDP, %{name: :bare_udp}},
      fn _x, acc -> {Strawberry.Api, %{}} <~> acc end
    )
  end

  def errorlayer do
    {Strawberry.ConstantErrorRate, %{}}
    <~> {Strawberry.Transmitter, %{name: :simple_transmitter}}
  end

  def errorlayer_udp do
    {Strawberry.ConstantErrorRate, %{}}
    <~> {Strawberry.BareUDP, %{name: :bare_udp}}
  end

  def speedlayer do
    {Strawberry.RateLimiter, %{rate: 250_000}}
    <~> {Strawberry.Transmitter, %{name: :simple_transmitter}}
  end
end
