defmodule Strawberry.Wifi do
  import Strawberry.LayerComposer

  def layer do
    {Strawberry.States, %{}}
    # Ethernet frame spec
    <~> {Strawberry.SizeLimiter, %{packet_size: 1500}}
    # 10Gb/sec
    <~> {Strawberry.RateLimiter, %{rate: :math.pow(10, 10)}}
    # 802 spec
    <~> {Strawberry.ConstantErrorRate, %{drop_fraction: :math.pow(10, -8)}}
    <~> {Strawberry.Transmitter, %{name: :wifi_transmitter}}
  end
end
