defmodule Strawberry.Nodes do
  require Strawberry.Component
  require Logger
  alias Strawberry.Component, as: Comp
  import Strawberry.LayerComposer

  def configs_old do
    [
      Comp.component Bulb1, [light = false], Strawberry.IP.layer() do
        on :on do
          Logger.info("Light turned on")
          light = true
        end

        on :off do
          light = false
        end
      end,
      Comp.component Bulb2,
                     [light = false],
                     one_way(
                       {Strawberry.SizeLimiter, %{name: :limiter, packet_size: 1500}}
                       # 10Gb/sec
                       ~> {Strawberry.RateLimiter, %{rate: :math.pow(10, 10)}}
                       # 802 spec
                       ~> {Strawberry.ConstantErrorRate, %{drop_fraction: :math.pow(10, -8)}}
                     )
                     <~> {Strawberry.Transmitter, %{name: :ip_transmitter}} do
        on :on do
          light = true
        end

        on :off do
          Logger.info("Light turned off")
          # limiter.packet_size = 1500
          light = false
        end
      end,
      Comp.component Switch1, [light = false], Strawberry.IP.layer() do
        every 5000 do
          light = !light
          Logger.info("Switching light to #{inspect(light)}")

          if light do
            send(Bulb1, :on)
          else
            send(Bulb1, :off)
          end
        end

        every 2000 do
          send(Bulb2, :off)
        end

        every 2200 do
          cast(:off)
        end
      end
    ]

    # |> inspect()
    # |> IO.puts()
  end

  def configs_sensornet do
    Comp.component_map [
                         {Sensor1,
                          Strawberry.MultiBluetooth.layer([:link_bs_s1, :link_s1_s2, :link_s1_s3])},
                         {Sensor2,
                          Strawberry.MultiBluetooth.layer([:link_bs_s2, :link_s1_s2, :link_s2_s3])},
                         {Sensor3,
                          Strawberry.MultiBluetooth.layer([
                            :link_bs_s3,
                            :link_s1_s3,
                            :link_s2_s3,
                            :link_s3_s4,
                            :link_s3_s5
                          ])},
                         {Sensor4,
                          Strawberry.MultiBluetooth.layer([:link_s3_s4, :link_s4_s5, :link_s4_s6])},
                         {Sensor5,
                          Strawberry.MultiBluetooth.layer([:link_s3_s5, :link_s4_s5, :link_s5_s6])},
                         {Sensor6, Strawberry.MultiBluetooth.layer([:link_s4_s6, :link_s5_s6])}
                       ],
                       [] do
      every 5000 do
        data = get_temperature_data()
        cast(data)
      end
    end ++
      [
        Comp.component AccessPoint,
                       [],
                       Strawberry.LayerComposer.multistack(
                         {Strawberry.Bluetooth, %{name: link_bs_s1}} |||
                           {Strawberry.Bluetooth, %{name: link_bs_s2}} |||
                           {Strawberry.Bluetooth, %{name: link_bs_s3}}
                       ) do
          on data do
            log_data(data)
          end
        end
      ]
  end

  def configs do
    [
      Comp.component Sender,
                     [
                       file = nil,
                       currentID = 0,
                       to_comfirm = %{}
                     ],
                     Strawberry.SimpleTransmitter.layer() do
        init do
          file = File.open("results.txt", [:write]) |> elem(1)
          IO.write(file, "ID, Latency, Size\n")
          Logger.info("starting")
        end

        every 50 do
          id = currentID
          Logger.info("sending ping #{inspect(id)}")
          # test = Map.keys(to_comfirm)
          # Logger.info("ping keys: #{inspect(test)}")
          pad_size = :rand.uniform(1024)
          padding = Strawberry.Padding.generate_pad(pad_size)
          to_comfirm = Map.put(to_comfirm, currentID, Time.utc_now())
          send(Receiver, {:ping, currentID, padding})
          delayed_trigger(1000, {:timeout, currentID, pad_size})
          currentID = currentID + 1
        end

        on {:pong, id, padding} do
          receive_time = Time.utc_now()
          {send_time, new_map} = Map.pop(to_comfirm, id)
          to_comfirm = new_map
          ellapsed = Time.diff(receive_time, send_time, :microseconds)
          size = Strawberry.Padding.find_size({:pong, id, padding})
          IO.write(file, "#{inspect(id)}, #{inspect(ellapsed)}, #{inspect(size)}\n")
          # test = Map.keys(to_comfirm)
          # Logger.info("pong keys: #{inspect(test)}")
        end

        on {:timeout, id, pad_size} do
          {send_time, new_map} = Map.pop(to_comfirm, id, false)
          to_comfirm = new_map

          if send_time do
            Logger.info("timeout: #{inspect(id)} #{inspect(send_time)}")
            IO.write(file, "#{inspect(id)}, -1, #{inspect(pad_size)}\n")
          end
        end
      end,
      Comp.component Receiver, [], Strawberry.SimpleTransmitter.layer() do
        on {:ping, id, padding} do
          send(Sender, {:pong, id, padding})
          Logger.info("received ping")
        end
      end
    ]
  end
end
