defmodule Strawberry.Padding do
  require Logger

  def generate_pad(n) do
    :crypto.strong_rand_bytes(n)
  end

  def find_size(term) do
    :erlang.byte_size(:erlang.term_to_binary(term))
  end
end
