defmodule Strawberry.Slave do
  use GenServer
  require Logger

  def start_link(arg) do
    GenServer.start_link(__MODULE__, arg, name: __MODULE__)
  end

  def init(state) do
    Logger.info("Starting up Slave")
    GenServer.cast({Strawberry.Distributor, state.address}, {:node_present, Node.self()})
    {:ok, state}
  end

  def handle_call({:initialize, component, args}, _from, state) do
    Strawberry.Nodes.configs()
    {:ok, _} = component.start_link(args)
    {:reply, nil, state}
  end
end
