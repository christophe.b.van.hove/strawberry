defmodule Strawberry.Util do
  require Logger

  # State helpers

  def state_check(state, name, default, info) do
    if not Map.has_key?(state, name) do
      if info, do: Logger.warn(info)
      Map.put(state, name, default)
    else
      state
    end
  end

  def state_present(state, name, module) do
    if not Map.has_key?(state, name) do
      Logger.warn("Missing value for state: #{inspect(name)} in #{inspect(module)}")
    end

    state
  end
end
