defmodule Strawberry.Mixfile do
  use Mix.Project

  def project do
    [
      app: :strawberry,
      version: "0.1.0",
      elixir: "~> 1.5",
      start_permanent: Mix.env() == :prod,
      deps: deps()
    ]
  end

  # Run "mix help compile.app" to learn about applications.
  def application do
    [
      extra_applications: [:logger, :erlang_term, :snowflake],
      mod: {Strawberry.Application, []}
    ]
  end

  # Run "mix help deps" to learn about dependencies.
  defp deps do
    [
      {:erlang_term, "~> 1.7"},
      {:snowflake, "~> 1.0.0"},
      {:msgpax, "~> 2.0"},
      {:msgpack, "~> 0.7.0"},
      {:private, "~> 0.1.1"},
      {:export, "~> 0.0.7"},
      # {:erlport, github: "hdima/erlport", manager: :make},
      {:socket, "~> 0.3"}
      # {:dep_from_hexpm, "~> 0.3.0"},
      # {:dep_from_git, git: "https://github.com/elixir-lang/my_dep.git", tag: "0.1.0"},
    ]
  end
end
