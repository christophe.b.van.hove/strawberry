defmodule LayerComposerTest do
  use ExUnit.Case
  import Strawberry.LayerComposer

  test "Atom Layer" do
    layer1 = atom_layer()
    layer2 = atom_layer()
    assert layer1 != layer2
  end

  test "Split Left Right" do
    assert split_left_right([1], [2]) == {{[], [1]}, {[2], []}}
    assert split_left_right([1, 2], [3, 4]) == {{[1], [2]}, {[3], [4]}}
    assert split_left_right([1, 2, 3], [4, 5, 6]) == {{[1, 2], [3]}, {[4], [5, 6]}}
  end

  test "Split Ends" do
    assert split_ends([1, 2]) == {[1], [], [2]}
    assert split_ends([1, 2, 3]) == {[1], [2], [3]}
    assert split_ends([1, 2, 3, 4]) == {[1], [2, 3], [4]}
    assert split_ends([1, 2, 3, 4, 5]) == {[1], [2, 3, 4], [5]}
  end

  test "Standard Operator" do
    layer1 = atom_layer()
    name1 = layer1 |> elem(1) |> Map.get(:name)
    layer2 = atom_layer()
    name2 = layer2 |> elem(1) |> Map.get(:name)
    assert layer1 != layer2

    assert layer1 <~> layer2 == [
             {Strawberry.Api, %{name: name1, lower_layer: name2, lower_mod: Strawberry.Api}},
             {Strawberry.Api, %{name: name2, upper_layer: name1, upper_mod: Strawberry.Api}}
           ]

    layer3 = atom_layer()
    name3 = layer3 |> elem(1) |> Map.get(:name)

    correct_three_list = [
      {Strawberry.Api, %{name: name1, lower_layer: name3, lower_mod: Strawberry.Api}},
      {Strawberry.Api,
       %{
         name: name3,
         upper_layer: name1,
         upper_mod: Strawberry.Api,
         lower_layer: name2,
         lower_mod: Strawberry.Api
       }},
      {Strawberry.Api, %{name: name2, upper_layer: name3, upper_mod: Strawberry.Api}}
    ]

    assert layer1 <~> layer3 <~> layer2 == correct_three_list
    assert layer1 <~> layer3 <~> layer2 == correct_three_list
    assert layer1 <~> (layer3 <~> layer2) == correct_three_list
  end

  test "One-way Operator" do
    layer1 = atom_layer()
    name1 = layer1 |> elem(1) |> Map.get(:name)
    layer2 = atom_layer()
    name2 = layer2 |> elem(1) |> Map.get(:name)

    assert layer1 ~> layer2 == [
             {Strawberry.Api,
              %{
                name: name1,
                lower_layer: name2,
                lower_mod: Strawberry.Api,
                upper_layer: name2,
                upper_mod: Strawberry.Api
              }},
             {Strawberry.Api, %{name: name2}}
           ]

    layer3 = atom_layer()
    name3 = layer3 |> elem(1) |> Map.get(:name)

    correct_three_list = [
      {Strawberry.Api,
       %{
         name: name1,
         lower_layer: name2,
         lower_mod: Strawberry.Api,
         upper_layer: name2,
         upper_mod: Strawberry.Api
       }},
      {Strawberry.Api,
       %{
         name: name2,
         lower_layer: name3,
         lower_mod: Strawberry.Api,
         upper_layer: name3,
         upper_mod: Strawberry.Api
       }},
      {Strawberry.Api, %{name: name3}}
    ]

    assert layer1 ~> layer2 ~> layer3 == correct_three_list
    assert layer1 ~> layer2 ~> layer3 == correct_three_list
    assert layer1 ~> (layer2 ~> layer3) == correct_three_list

    layer4 = atom_layer()
    name4 = layer4 |> elem(1) |> Map.get(:name)
    layer5 = atom_layer()
    name5 = layer5 |> elem(1) |> Map.get(:name)

    one_way_segment = one_way(layer2 ~> layer3 ~> layer4)
    name_atom_upper = one_way_segment |> Enum.at(0) |> elem(1) |> Map.get(:name)
    name_atom_lower = one_way_segment |> Enum.at(4) |> elem(1) |> Map.get(:name)

    correct_five_list = [
      {Strawberry.Api,
       %{
         name: name1,
         lower_layer: name_atom_upper,
         lower_mod: Strawberry.Api
       }},
      {Strawberry.Api,
       %{
         name: name_atom_upper,
         lower_layer: name2,
         lower_mod: Strawberry.Api,
         upper_layer: name1,
         upper_mod: Strawberry.Api
       }},
      {Strawberry.Api,
       %{
         name: name2,
         lower_layer: name3,
         lower_mod: Strawberry.Api,
         upper_layer: name3,
         upper_mod: Strawberry.Api
       }},
      {Strawberry.Api,
       %{
         name: name3,
         lower_layer: name4,
         lower_mod: Strawberry.Api,
         upper_layer: name4,
         upper_mod: Strawberry.Api
       }},
      {Strawberry.Api,
       %{
         name: name4,
         lower_layer: name_atom_lower,
         lower_mod: Strawberry.Api,
         upper_layer: name_atom_upper,
         upper_mod: Strawberry.Api
       }},
      {Strawberry.Api,
       %{
         name: name_atom_lower,
         lower_layer: name5,
         lower_mod: Strawberry.Api,
         upper_layer: name2,
         upper_mod: Strawberry.Api
       }},
      {Strawberry.Api,
       %{
         name: name5,
         upper_layer: name_atom_lower,
         upper_mod: Strawberry.Api
       }}
    ]

    # IO.inspect([
    #  {"layer1", layer1},
    #  {"layerapi", name_atom_upper},

    #  {"layer2", layer2},
    #  {"layer3", layer3},
    #  {"layer4", layer4},
    #  {"layerapi", name_atom_lower},
    #  {"layer5", layer5}
    # ])

    assert layer1 <~> one_way_segment <~> layer5 == correct_five_list

    one_way_segment_2 = one_way(layer2)
    name_atom_upper = one_way_segment_2 |> Enum.at(0) |> elem(1) |> Map.get(:name)
    name_atom_lower = one_way_segment_2 |> Enum.at(2) |> elem(1) |> Map.get(:name)

    correct_three_list_2 = [
      {Strawberry.Api,
       %{
         name: name1,
         lower_layer: name_atom_upper,
         lower_mod: Strawberry.Api
       }},
      {Strawberry.Api,
       %{
         name: name_atom_upper,
         lower_layer: name2,
         lower_mod: Strawberry.Api,
         upper_layer: name1,
         upper_mod: Strawberry.Api
       }},
      {Strawberry.Api,
       %{
         name: name2,
         lower_layer: name_atom_lower,
         lower_mod: Strawberry.Api,
         upper_layer: name_atom_upper,
         upper_mod: Strawberry.Api
       }},
      {Strawberry.Api,
       %{
         name: name_atom_lower,
         lower_layer: name3,
         lower_mod: Strawberry.Api,
         upper_layer: name2,
         upper_mod: Strawberry.Api
       }},
      {Strawberry.Api,
       %{
         name: name3,
         upper_layer: name_atom_lower,
         upper_mod: Strawberry.Api
       }}
    ]

    assert layer1 <~> one_way_segment_2 <~> layer3 == correct_three_list_2
  end

  test "Multistack Operator" do
    stack1 = atom_layer() <~> atom_layer() <~> atom_layer()
    name_upper_1 = stack1 |> Enum.at(0) |> elem(1) |> Map.get(:name)
    name_lower_1 = stack1 |> Enum.at(2) |> elem(1) |> Map.get(:name)
    stack2 = atom_layer() <~> atom_layer()
    name_upper_2 = stack2 |> Enum.at(0) |> elem(1) |> Map.get(:name)
    name_lower_2 = stack2 |> Enum.at(1) |> elem(1) |> Map.get(:name)
    stack3 = [atom_layer()]
    name_3 = stack1 |> Enum.at(0) |> elem(1) |> Map.get(:name)

    stacks = stack1 ||| stack2 ||| stack3

    assert stacks == [stack1, stack2, stack3]
  end

  test "Multistack" do
    layer1 = atom_layer()
    name1 = layer1 |> elem(1) |> Map.get(:name)
    layer2 = atom_layer()
    name2 = layer2 |> elem(1) |> Map.get(:name)
    layer3 = atom_layer()
    name3 = layer3 |> elem(1) |> Map.get(:name)

    double_ended =
      multistack(%{:mode => :spread}, layer1 ||| layer2 ||| layer3, %{:mode => :spread})

    IO.inspect(double_ended)
    translator_name_1a = double_ended |> Enum.at(1) |> elem(1) |> Map.get(:name)
    translator_name_1b = double_ended |> Enum.at(3) |> elem(1) |> Map.get(:name)
    translator_name_2a = double_ended |> Enum.at(4) |> elem(1) |> Map.get(:name)
    translator_name_2b = double_ended |> Enum.at(6) |> elem(1) |> Map.get(:name)
    translator_name_3a = double_ended |> Enum.at(7) |> elem(1) |> Map.get(:name)
    translator_name_3b = double_ended |> Enum.at(9) |> elem(1) |> Map.get(:name)

    double_ended_correct = [
      {Strawberry.LayerBridge,
       %{
         lower_layer: [translator_name_1a, translator_name_2a, translator_name_3a],
         mode: :spread
       }},
      {Strawberry.BridgeTranslator,
       %{
         direction: true,
         id: 0,
         lower_layer: name1,
         lower_mod: Strawberry.Api,
         name: translator_name_1a
       }},
      {Strawberry.Api,
       %{
         lower_layer: translator_name_1b,
         lower_mod: Strawberry.BridgeTranslator,
         name: name1,
         upper_layer: translator_name_1a,
         upper_mod: Strawberry.BridgeTranslator
       }},
      {Strawberry.BridgeTranslator,
       %{
         direction: false,
         id: 0,
         name: translator_name_1b,
         upper_layer: name1,
         upper_mod: Strawberry.Api
       }},
      {Strawberry.BridgeTranslator,
       %{
         direction: true,
         id: 1,
         lower_layer: name2,
         lower_mod: Strawberry.Api,
         name: translator_name_2a
       }},
      {Strawberry.Api,
       %{
         lower_layer: translator_name_2b,
         lower_mod: Strawberry.BridgeTranslator,
         name: name2,
         upper_layer: translator_name_2a,
         upper_mod: Strawberry.BridgeTranslator
       }},
      {Strawberry.BridgeTranslator,
       %{
         direction: false,
         id: 1,
         name: translator_name_2b,
         upper_layer: name2,
         upper_mod: Strawberry.Api
       }},
      {Strawberry.BridgeTranslator,
       %{
         direction: true,
         id: 2,
         lower_layer: name3,
         lower_mod: Strawberry.Api,
         name: translator_name_3a
       }},
      {Strawberry.Api,
       %{
         lower_layer: translator_name_3b,
         lower_mod: Strawberry.BridgeTranslator,
         name: name3,
         upper_layer: translator_name_3a,
         upper_mod: Strawberry.BridgeTranslator
       }},
      {Strawberry.BridgeTranslator,
       %{
         direction: false,
         id: 2,
         name: translator_name_3b,
         upper_layer: name3,
         upper_mod: Strawberry.Api
       }},
      {Strawberry.LayerBridge,
       %{
         mode: :spread,
         upper_layer: [translator_name_1b, translator_name_2b, translator_name_3b]
       }}
    ]

    assert double_ended == double_ended_correct
  end
end
